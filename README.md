### README [v1.0], 14th Feb 2018 ###
**Enigmaki** is an **puzzle game** where one player controls a koala and has to place makis 
on their corresponding plates to progress and (maybe) find a way back home.

The game has an **isometric 2D** view, and is targeted to **children** (6 to 12 y.o, both genders, worldwide),
on **mobile, tablet, and PC** (iOS, Android, Windows, macOS, Linux).

For more details please visit the link mentioned below.
[https://www.notion.so/imaginaryfriendssociety/Enigmaki-215b2157d4354f999040d2fa8be359c5]

### What is this repository for? ###
This is the main repository for Enigmaki. All official changes to the game made by our team will be stored here.
Check out our project page for details regarding your eligibility for source code modifications and forking this 
repository (you probably are :p)

### How do I get set up? ###
Check out tutorials on how to fork a repository online. Once you do, you'll need the tools mentioned below to 
start modifying the project - 
Unity 2017.3.0f3 - Game Engine
Visual Studio 2017 Community Edition (recommended)

You can also use Monodevelop or any other IDE of your choice which supports MS Visual C# language.

### Contribution guidelines ###
So, you've decided that you want to contribute to the project. We advice you to get in touch with one of our 
team members using the email address below.

### Who do I talk to? ###
Have something to say? Get in touch with us at the email ID below.
[please.reply@imaginary-friends-society.com]

You can also find more information about our team at our project page.
[https://www.notion.so/imaginaryfriendssociety/Imaginary-Friends-01ec3bc5649e482aaaec77e32a920e5f]