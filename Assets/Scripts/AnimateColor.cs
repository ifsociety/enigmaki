﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AnimateColor : MonoBehaviour
{
    // getting a reference to sprite
    public Image maki;
    [SerializeField]
    private Color startCol;
    [SerializeField]
    private Color endColor;

    // controlling the animation
    public float duration;
    public bool isSpriteColorAnimated;
    private IEnumerator animCol;

	// Use this for initialization
	void Start ()
    {
        // assign enumerator
        animCol = AnimateImageColor();
        StartCoroutine(animCol);
	}

    // enumerator for animating the color property of the sprite
    private IEnumerator AnimateImageColor()
    {
        while (isSpriteColorAnimated == true)
        {
            float startTime = Time.time;
            float percent = 0.0f;
            while (percent <= 1.0f)
            {
                percent = (Time.time - startTime) / duration;
                maki.color = Color.Lerp(startCol, endColor, percent);
                yield return new WaitForEndOfFrame();
            }

            // now color needs to be reversed
            startTime = Time.time;
            percent = 0.0f;
            while (percent >= 0)
            {
                percent = 1.0f - ((Time.time - startTime) / duration);
                maki.color = Color.Lerp(startCol, endColor, percent);
                yield return new WaitForEndOfFrame();
            }
        }

        yield return null;
    }
}